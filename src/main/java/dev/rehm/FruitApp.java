package dev.rehm;

import dev.rehm.models.Fruit;

public class FruitApp {

    public static void main(String[] args) {
        JsonMapper mapper = new JsonMapper();
        Fruit fruit = new Fruit("green",100,false,"banana");
        String fruitJson = mapper.serialize(fruit);
        System.out.println(fruitJson);
    }
}

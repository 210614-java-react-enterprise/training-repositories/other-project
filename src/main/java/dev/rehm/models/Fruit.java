package dev.rehm.models;

import java.util.Objects;

public class Fruit {

    private String color;
    private int weight;
    private boolean isRipe;
    private String type;

    public Fruit() {
    }

    public Fruit(String color, int weight, boolean isRipe, String type) {
        this.color = color;
        this.weight = weight;
        this.isRipe = isRipe;
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public boolean getIsRipe() {
        return isRipe;
    }

    public void setIsRipe(boolean ripe) {
        isRipe = ripe;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fruit fruit = (Fruit) o;
        return weight == fruit.weight && isRipe == fruit.isRipe && Objects.equals(color, fruit.color) && Objects.equals(type, fruit.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, weight, isRipe, type);
    }

    @Override
    public String toString() {
        return "Fruit{" +
                "color='" + color + '\'' +
                ", weight=" + weight +
                ", isRipe=" + isRipe +
                ", type='" + type + '\'' +
                '}';
    }
}
